# Azure Devops Agent Docker Images using alt authentication (Username/Password)

## Build the image

Run the following command within that directory:

    docker build -t azdevops-agent-alt:latest .

This command builds the `Dockerfile` in the current directory.

* The final image is tagged `azdevops-agent-alt:latest`. You can easily run it in a container as `azdevops-agent-alt`, since the latest tag is the default if no tag is specified.

## Start the image

Now that you have created an image, you can spin up a container.

Open a terminal

Run the container. This will install the latest version of the agent, configure it, and run the agent targeting the Default pool of a specified Azure DevOps or Azure DevOps Server instance of your choice:

    docker run -e AZP_URL=<Azure DevOps instance> -e AZP_USER=<Azure DevOps Username> -e AZP_PASSWD<Azure DevOps Password> -e AZP_AGENT_NAME=mydockeragent azdevops-agent-alt:latest

## Environment variables
You can optionally control the pool and agent work directory using additional environment variables.

| Environment variable | Description |
| :------------------  | :----------|
| `AZP_URL`            | The URL of the Azure DevOps or Azure DevOps Server instance (Example: `https://dev.azure.com/myorganization` or `http://my-azure-devops-server:8080/tfs`) |
| `AZP_USER` 	       | Username for granting access to `AZP_URL` (Example: `domain\userName` or `userName@domain.com`) |
| `AZP_PASSWD` 	       | Password for granting access to `AZP_URL` |
| `AZP_AGENT_NAME` 	   | Agent name (default value: `the container hostname`) |
| `AZP_POOL`           | Agent pool name (default value: `Default`) |
| `AZP_WORK`           | Work directory (default value: `_work`) |

## Adding tools and customizing the container

In this walkthrough, you created a basic build agent. You can extend the `Dockerfile` to include additional tools and their dependencies, or build your own container using this one as a base layer. 

Just make sure that the following things are left untouched:
*  The `start.sh` script is called by the `Dockerfile`
*  The `start.sh` script is the last command that the `Dockerfile`
*  Ensure that derivative containers do not remove any of the dependencies stated by the `Dockerfile`

## Using Docker within a Docker container

In order to use Docker from within a Docker container, you need to bind-mount the Docker socket. This has very serious security implications - namely, code inside the container can now run as root on your Docker host. If you're sure you want to do this, see the [bind mount](https://docs.docker.com/storage/bind-mounts/) documentation on Docker.com.


